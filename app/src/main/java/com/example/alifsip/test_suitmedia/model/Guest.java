package com.example.alifsip.test_suitmedia.model;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;

import com.example.alifsip.test_suitmedia.R;

/**
 * Created by alif.sip on 13/10/2016.
 */

public class Guest {
    private String guest_name, birthdate;
    private int image;

    public Guest(String guest_name, String birthdate) {
        this.guest_name = guest_name;
        this.birthdate = birthdate;
        this.image = R.drawable.guest;
    }

    public String getGuest_name() {
        return guest_name;
    }

    public void setGuest_name(String guest_name) {
        this.guest_name = guest_name;
    }

    public String getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(String birthdate) {
        this.birthdate = birthdate;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }
}

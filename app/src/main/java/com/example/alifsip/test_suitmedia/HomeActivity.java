package com.example.alifsip.test_suitmedia;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class HomeActivity extends AppCompatActivity {

    private Button btn_submitName;
    private EditText edt_name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        btn_submitName = (Button) findViewById(R.id.btn_submitName);
        edt_name = (EditText) findViewById(R.id.edt_name);

        btn_submitName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomeActivity.this, EventOrGuestActivity.class);
                intent.putExtra("key_name", edt_name.getText().toString());
                startActivity(intent);
            }
        });
    }
}

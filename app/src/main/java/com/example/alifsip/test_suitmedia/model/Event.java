package com.example.alifsip.test_suitmedia.model;

import android.graphics.Bitmap;

/**
 * Created by alif.sip on 12/10/2016.
 */

public class Event {
    private String event_name, tanggal;
    private Bitmap image;

    public Event(Bitmap image, String event_name, String tanggal) {
        this.image = image;
        this.event_name = event_name;
        this.tanggal = tanggal;
    }

    public String getEvent_name() {
        return event_name;
    }

    public void setEvent_name(String event_name) {
        this.event_name = event_name;
    }

    public String getTanggal() {
        return tanggal;
    }

    public void setTanggal(String tanggal) {
        this.tanggal = tanggal;
    }

    public Bitmap getImage() {
        return image;
    }

    public void setImage(Bitmap image) {
        this.image = image;
    }

}

package com.example.alifsip.test_suitmedia;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.alifsip.test_suitmedia.model.Event;
import com.example.alifsip.test_suitmedia.model.Guest;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by alif.sip on 13/10/2016.
 */

public class GridViewAdapter extends ArrayAdapter<Guest> {

    public GridViewAdapter(Context context, List<Guest> guests) {
        super(context, 0, guests);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.guest_grid_item, parent, false);
        }

        Guest guest = getItem(position);

        ImageView img_cover = (ImageView) convertView.findViewById(R.id.img_guest);
        TextView txt_guest = (TextView) convertView.findViewById(R.id.txt_guest);

        img_cover.setImageResource(guest.getImage());
        txt_guest.setText(guest.getGuest_name());

        return convertView;
    }
}

package com.example.alifsip.test_suitmedia;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class EventOrGuestActivity extends AppCompatActivity {

    private TextView txt_name;
    private Button btn_selectEvent, btn_selectGuest;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_or_guest);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        txt_name = (TextView) findViewById(R.id.txt_name);
        btn_selectEvent = (Button) findViewById(R.id.btn_selectEvent);
        btn_selectGuest = (Button) findViewById(R.id.btn_selectGuest);

        setName();

        btn_selectEvent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(EventOrGuestActivity.this, EventActivity.class);
                startActivityForResult(intent, 1);
            }
        });

        btn_selectGuest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(EventOrGuestActivity.this, GuestActivity.class);
                startActivityForResult(intent, 2);
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            if (resultCode == RESULT_OK) {
                btn_selectEvent.setText(data.getStringExtra("selected_event"));
            }
        } else if (requestCode == 2) {
            if (resultCode == RESULT_OK) {
                btn_selectGuest.setText(data.getStringExtra("selected_guest"));
                String[] birthdate = data.getStringExtra("selected_tanggal_guest").split("-");
                String phone_type;

                if (Integer.parseInt(birthdate[2]) % 2 == 0)
                    phone_type = "blackberry";
                else if (Integer.parseInt(birthdate[2]) % 3 == 0 && Integer.parseInt(birthdate[2]) % 2 == 0)
                    phone_type = "iOS";
                else if (Integer.parseInt(birthdate[2]) % 3 == 0)
                    phone_type = "android";
                else phone_type = "feature phone";

                Toast.makeText(this, phone_type, Toast.LENGTH_LONG).show();
            }
        }
    }

    private void setName() {
        txt_name.setText("Nama : " + getIntent().getExtras().getString("key_name", null));
    }
}

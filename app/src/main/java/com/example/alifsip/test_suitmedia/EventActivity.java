package com.example.alifsip.test_suitmedia;

import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.alifsip.test_suitmedia.model.Event;

import java.util.ArrayList;
import java.util.List;

public class EventActivity extends AppCompatActivity {
    private ListView listView;
    private EventListAdapter listAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event);

        listView = (ListView) findViewById(R.id.list);
        setDummy();
        listView.setAdapter(listAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                Event selected_event = listAdapter.getItem(position);

                Intent intent = new Intent();
                intent.putExtra("selected_event", selected_event.getEvent_name());
                setResult(RESULT_OK, intent);
                finish();
            }
        });
    }

    private void setDummy() {
        Event event = new Event(BitmapFactory.decodeResource(this.getResources(), R.drawable.satu), "abc event", "15 Sept 2014");
        Event event1 = new Event(BitmapFactory.decodeResource(this.getResources(), R.drawable.dua), "def event", "27 Sept 2014");
        Event event2 = new Event(BitmapFactory.decodeResource(this.getResources(), R.drawable.tiga), "ghi event", "3 Des 2014");
        Event event3 = new Event(BitmapFactory.decodeResource(this.getResources(), R.drawable.empat), "jkl event", "5 jan 2015");

        List<Event> events = new ArrayList<>();
        events.add(event);
        events.add(event1);
        events.add(event2);
        events.add(event3);
        listAdapter = new EventListAdapter(this, events);
    }
}

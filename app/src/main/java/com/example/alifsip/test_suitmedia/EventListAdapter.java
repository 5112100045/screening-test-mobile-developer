package com.example.alifsip.test_suitmedia;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.alifsip.test_suitmedia.model.Event;

import java.util.List;

/**
 * Created by alif.sip on 12/10/2016.
 */

public class EventListAdapter extends ArrayAdapter<Event> {

    public EventListAdapter(Context context, List<Event> events) {
        super(context, 0, events);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View listItemView = convertView;
        if (listItemView == null) {
            listItemView = LayoutInflater.from(getContext()).inflate(R.layout.event_list_item, parent, false);
        }

        Event event = getItem(position);

        TextView txt_event = (TextView) listItemView.findViewById(R.id.txt_event);
        TextView txt_tanggal = (TextView) listItemView.findViewById(R.id.txt_tanggal);
        ImageView img_event = (ImageView) listItemView.findViewById(R.id.img_event);

        txt_event.setText(event.getEvent_name());
        txt_tanggal.setText(event.getTanggal());
        img_event.setImageBitmap(event.getImage());

        return listItemView;
    }
}

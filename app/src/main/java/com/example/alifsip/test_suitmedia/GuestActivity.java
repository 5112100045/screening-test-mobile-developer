package com.example.alifsip.test_suitmedia;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.Toast;

import com.example.alifsip.test_suitmedia.model.Event;
import com.example.alifsip.test_suitmedia.model.Guest;

import java.util.ArrayList;
import java.util.List;

public class GuestActivity extends AppCompatActivity {

    private static final String REQUEST_URL =
            "http://dry-sierra-6832.herokuapp.com/api/people";
    private GridViewAdapter gridViewAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_guest);

//        Guest guest = new Guest("abc event", "15 Sept 2014");
//        Guest guest2 = new Guest("abc event", "15 Sept 2014");
//        Guest guest3 = new Guest("abc event", "15 Sept 2014");
//        Guest guest4 = new Guest("abc event", "15 Sept 2014");
//        Guest guest5 = new Guest("abc event", "15 Sept 2014");
//
//        List<Guest> guests = new ArrayList<>();
//        guests.add(guest);
//        guests.add(guest2);
//        guests.add(guest3);
//        guests.add(guest4);
//        guests.add(guest5);
//        gridViewAdapter = new GridViewAdapter(this, guests);

        final GridView gridView = (GridView) findViewById(R.id.grid_view);
        gridViewAdapter = new GridViewAdapter(this, new ArrayList<Guest>());
        gridView.setAdapter(gridViewAdapter);

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                Guest selected_guest = gridViewAdapter.getItem(position);

                Intent intent = new Intent();
                intent.putExtra("selected_guest", selected_guest.getGuest_name());
                intent.putExtra("selected_tanggal_guest", selected_guest.getBirthdate());
                setResult(RESULT_OK, intent);
                finish();
            }
        });


       new GuestAsyncTask().execute(REQUEST_URL);
    }

    private class GuestAsyncTask extends AsyncTask<String, Void, List<Guest>> {

        private ProgressDialog dialog;

        @Override
        protected void onPreExecute() {
            dialog = new ProgressDialog(GuestActivity.this);
            dialog.setMessage("Please Wait...");
            dialog.setCanceledOnTouchOutside(false);
            dialog.show();
        }

        @Override
        protected List<Guest> doInBackground(String... urls) {
            if (urls.length < 1 || urls[0] == null) {
                return null;
            }

            List<Guest> result = QueryUtils.fetchListData(urls[0]);
            return result;
        }

        @Override
        protected void onPostExecute(List<Guest> data) {
            // Clear the adapter of previous data
            gridViewAdapter.clear();

            if (data != null && !data.isEmpty()) {
                gridViewAdapter.addAll(data);
            }
            else{
                Toast.makeText(getApplicationContext(), "Error Connection", Toast.LENGTH_LONG).show();
                finish();
            }

            if (dialog.isShowing()) {
                dialog.dismiss();
            }
        }
    }
}
